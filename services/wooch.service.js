const setAdmin = (uuid, b)=> {
  return woochClient.post(`/admin/users/${uuid}/role`, {
    admin: b
  })
}

const axios = require('axios');
const cache = require('./cache.service');
const {createHmac} = require('crypto');

const {
  WOOCH_URL, WOOCH_PUSH_KEY, WOOCH_PUSH_KEY_SECRET, WOOCH_ACCESS_KEY, WOOCH_SIMDE_KEY_SECRET, WOOCH_SIMDE_KEY
} = process.env;

const woochClient = axios.create({
  baseURL: WOOCH_URL, headers: {
    'X-Api-Key': WOOCH_ACCESS_KEY,
    'Authorization': 'Bearer ' + WOOCH_PUSH_KEY,
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  }
})

woochClient.interceptors.request.use(config => {
  //make an hash of the request body
  const hash = createHmac('sha256', WOOCH_PUSH_KEY_SECRET);
  //order keys to have the same hash for the same data
  let data = config.data ?? {key: WOOCH_PUSH_KEY};
  hash.update(JSON.stringify(data));
  config.headers['X-Hash'] = hash.digest('base64');
  return config;
})

const listMarkets = async () => {
  const response = await woochClient.get('/admin/markets');
  return response.data;
}

const createMarket = async (name, slug, email, short_description) => {
  const response = await woochClient.post('/admin/markets', {
    name, slug, email, short_description, active: true
  });
  return response.data;
}

const editMarket = async (id, data) => {
  const response = await woochClient.patch(`/admin/markets/${id}`, {
    ...data
  });
  return response.data;
}

async function createAxiosSimdeAccount() {
  return cache.pRemember('simdeAccessToken', async () => {
    console.log("[Wooch-client] Creating access token")
    const response = await axios.post(WOOCH_URL + 'auth/connect', {
      method: "api-key", key: WOOCH_SIMDE_KEY, secret: WOOCH_SIMDE_KEY_SECRET
    }).catch((s) => null);
    if (!response) return null;
    return response.data.auth.access_token;
  }, 1000 * 60 * 59).then((token) => {
    if(!token) throw "Unable to create simde access token";
    return axios.create({
      baseURL: WOOCH_URL, headers: {
        'X-Api-Key': WOOCH_ACCESS_KEY,
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    });
  });
}

const syncList = async (id, market, list) => {
  const client = await createAxiosSimdeAccount().catch((err) => console.error(err));
  if (!client) throw "Unable to create correct simde client";
  console.log("Starting sending to server");
  const response = await client.post(`market/${market}/memberships/${id}/members/sync`, {
    emails: list, method: 'sync'
  }).catch((err) => console.error(err.response.data))
  return response.data;
}
const removeUserAccesses = async (uuid)=>{
    const response = await woochClient.delete(`/admin/users/${uuid}/access`);
    return response.data;
}
const addUserAccess = async (uuid, market_id, roles)=>{
    const response = await woochClient.post(`/admin/users/${uuid}/access`, {
        market_id, permissions: roles
    });
    return response.data;
}

const getAllUserIdWithAccess = async () => {
    const response = await woochClient.get('/admin/users/access');
    return response.data;
}
const removeUserAccess = async (uuid,market) => {
  const response = await woochClient.delete(`/admin/users/${uuid}/access/${market}`);
  return response.data;
}

const setTaskState = async (command, status, output) => {
  const response = await woochClient.post(`/admin/scheduler/tasks`, {
    command, status, output
  });
  return response.data;
}

module.exports = {
  listMarkets, createMarket, editMarket, syncList,removeUserAccesses,addUserAccess,getAllUserIdWithAccess,removeUserAccess,setAdmin,setTaskState
}