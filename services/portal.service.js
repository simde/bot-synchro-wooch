const axios = require('axios');
const mysql = require("mysql2/promise");
const {PORTAL_API_URL, DB_USER, DB_PASSWORD, DB_HOST} = process.env;
const portalClient = axios.create({
  baseURL: PORTAL_API_URL
});

const createConnection = async () => {
  return mysql.createConnection({
    host: DB_HOST, user: DB_USER, password: DB_PASSWORD, database: 'portail'
  });
}

const roleMapConverterJson = require('../converter.json').market;
const getConvertedRole = (role) => {
  return roleMapConverterJson[role] || [];
};
const getUsersAndRole = async () => {
  const conn = await createConnection();
  const [rows] = await conn.query("select a.login as assos_slug, u.email as uuid, am.role_id as role from assos_members am join portail.assos a on a.id = am.asso_id join portail.semesters s on s.id = am.semester_id join portail.users u on am.user_id = u.id where am.validated_by_id is not null and s.begin_at < now() && s.end_at > now()");
  conn.destroy();
  //current object is {assos_slug, uuid, role}[], we want to have {uuid: {assos_slug: [roles]}}
  let users = {};
  for (let row of rows) {
    // Check if the user is already in the users object, if not, initialize it
    if (!users[row.uuid]) {
      users[row.uuid] = {
        converted: {}, // To store converted roles
        normal: {} // To store normal roles
      };
    }

    // Get the converted role(s) for the current role
    const convertedRole = getConvertedRole(row.role);

    // If there are no converted roles, skip to the next iteration
    if (convertedRole.length === 0) {
      continue;
    }

    // Initialize the converted roles array for the association if it doesn't exist
    if (!users[row.uuid].converted[row.assos_slug]) {
      users[row.uuid].converted[row.assos_slug] = [];
    }

    // Initialize the normal roles array for the association if it doesn't exist
    if (!users[row.uuid].normal[row.assos_slug]) {
      users[row.uuid].normal[row.assos_slug] = [];
    }

    // Add the converted roles to the user's converted roles for the association
    users[row.uuid].converted[row.assos_slug].push(...convertedRole);

    // Add the normal role to the user's normal roles for the association
    users[row.uuid].normal[row.assos_slug].push(row.role);
  }
  return users;
}

const listAssociations = async () => {
  const response = await portalClient.get('assos');
  return response.data;
}

module.exports = {
  listAssociations,getUsersAndRole
}