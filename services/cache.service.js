//cache service store as a json file the datas
const fs = require('fs');
const crypto = require('crypto');
const path = require('path');
const {CACHE_PATH, WOOCH_ACCESS_KEY} = process.env;

let cache = {};
let iv = null;

const getCachePathKey = () => {
  if (!CACHE_PATH) {
    return path.join(__dirname, '..', 'cache.json');
  }
  return CACHE_PATH;
}

const getRandomIv = () => {
  let iv = Buffer.from(crypto.randomBytes(8))
  return iv.toString('hex');
}

const getCache = () => {
  try {
    const file = fs.readFileSync(getCachePathKey());
    let data = JSON.parse(file.toString("utf8"));
    iv = data.iv || getRandomIv();
    const encryptedData = data.encryptedData;
    cache = getCacheData(encryptedData, iv);
    return cache;
  } catch (error) {
    if (error.code === 'ENOENT') {
      //create file
      fs.writeFileSync(getCachePathKey(), JSON.stringify({}));
    }
    return cache = {};
  }
}

const setCache = (data) => {
  if (!iv) {
    getCache();
  }
  data = setCacheData(data, iv);
  fs.writeFileSync(getCachePathKey(), JSON.stringify({encryptedData: data, iv}));
}

//this function decrypt the cache file and return the data
const getCacheData = (data, iv) => {
  try {
    const decipher = crypto.createDecipheriv('aes-256-cbc', getKey(), iv);
    let decrypted = decipher.update(data, 'base64');
    decrypted += decipher.final('utf8');
    return JSON.parse(decrypted);
  } catch (error) {
    if (error.code === 'ENOENT') {
      return {};
    }
    console.warn("[cache-service] Error while reading cache file", error);
    return {};
  }
}

const getKey = () => {
  return WOOCH_ACCESS_KEY.slice(0, 32).padEnd(32, '5');
}
//this function encrypt the data
const setCacheData = (data, iv) => {
  try {
    const cipher = crypto.createCipheriv('aes-256-cbc', getKey(), iv);
    let encrypted = cipher.update(JSON.stringify(data), 'utf8', 'base64');
    encrypted += cipher.final('base64');
    return encrypted;
  } catch (error) {
    console.warn("[cache-service] Error while writing cache file", error);
  }
}

//get, set and delete functions with a key and expiry time
const get = (key) => {
  if (!cache[key]) {
    cache[key] = getCache()[key];
  }
  if (cache[key] && cache[key].expiry > Date.now()) {
    return cache[key].value;
  }
  delete cache[key];
  return null;
}

const set = (key, value, expiry_in = null) => {
  if (expiry_in === null) {
    expiry_in = 1000 * 60 * 60 * 24; // 24h
  }
  cache[key] = {
    value, expiry: Date.now() + expiry_in
  };
  setCache(cache);
}

const del = (key) => {
  delete cache[key];
  setCache(cache);
}

const remember = (key, callback, expiry_in = null) => {
  if (get(key)) {
    return get(key);
  }
  const value = callback();
  set(key, value, expiry_in);
  return value;
}
const pRemember = (key, callback, expiry_in = null) => {
  return new Promise((resolve, reject) => {
    if (get(key)) {
     return  resolve(get(key));
    }
    callback().then((value) => {
      set(key, value, expiry_in);
      resolve(value);
    }).catch(reject);
  });
}
module.exports = {
  get, set, remove: del, remember, pRemember
}