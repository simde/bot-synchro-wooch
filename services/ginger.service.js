const mysql = require('mysql2/promise');
const {DB_USER, DB_PASSWORD, DB_HOST} = process.env;
const createConnection = async () => {
  return mysql.createConnection({
      host: DB_HOST, user: DB_USER, password: DB_PASSWORD, database: 'utcotiz'
  });
}

const listCotizEmailsAndDates = async () => {
  const con = await createConnection();
  const [rows] = await con.query("SELECT * FROM membership_mails");
  con.destroy();
  return rows;
}
const listStudents = async () => {
  //SELECT * FROM mails_students
  const con = await createConnection();
  const [rows] = await con.query("SELECT email FROM student_mails");
  con.destroy();
  return rows.map(r => ({email: r.email}));
}
module.exports = {
  listCotizEmailsAndDates,listStudents
}