FROM node:18-alpine as system
WORKDIR /app
COPY . /app
RUN npm install
CMD ["npm", "run", "start-cron"]