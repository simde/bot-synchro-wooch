//env need vars PUSH_KEY, PUSH_KEY_SECRET, WOOCH_URL, PORTAL_API_URL, GINGER_URL

//doctor.js is a command that checks if the bot is ready to run (env vars are set, url are reachable)

const axios = require('axios');
const mysql = require('mysql2/promise');
let errorFlag = false;
const envVars = {
  'WOOCH_PUSH_KEY': {
    description: "To push admin confs", affect: ["sync-access", "sync-markets"]
  }, 'WOOCH_PUSH_KEY_SECRET': {
    description: "To sigh payload sent to wooch admin system with the push key", affect: ["sync-access", "sync-markets"]
  }, 'WOOCH_ACCESS_KEY': {
    description: "Key to access to wooch organisation & markets parts",
    affect: ["sync-access", "sync-markets", "sync-lists"]
  }, 'WOOCH_URL': {
    description: "Url to query wooch api", affect: ["sync-access", "sync-markets", "sync-lists"]
  }, 'PORTAL_API_URL': {
    description: "Url to query portal api", affect: ["sync-markets"]
  }, 'WOOCH_SIMDE_KEY': {
    description: "Simde wooch market access key", affect: ["sync-lists"]
  }, 'WOOCH_SIMDE_KEY_SECRET': {
    description: "Simde wooch market secret key", affect: ["sync-lists"]
  }, 'DB_USER': {
    description: "Database user to read ginger and portal deep infos", affect: ["sync-lists", "sync-access"]
  }, 'DB_PASSWORD': {
    description: "Database password to read ginger and portal deep infos", affect: ["sync-lists", "sync-access"]
  }, 'DB_HOST': {
    description: "Database host to read ginger and portal deep infos", affect: ["sync-lists", "sync-access"]
  }
};

const urls = {
  "WOOCH_URL": process.env.WOOCH_URL, "PORTAL_API_URL": process.env.PORTAL_API_URL
};

const checkEnvVars = () => {
  console.log("Env check...")
  let affected = [];
  for (const [name, infos] of Object.entries(envVars)) {
    if (!process.env[name]) {
      console.error(` - Variable ${name} is missing.`)
      affected.push(...infos.affect);
    }
  }
  //unique values
  affected = affected.filter((value, index, self) => self.indexOf(value) === index);
  if (affected.length !== 0) {
    console.error(`Commands : ${affected.join(', ')} couldn't be used`)
    errorFlag = true;
  }
}

const checkUrls = async () => {
  console.log("Url access check...")
  for (const [name, url] of Object.entries(urls)) {
    try {
      await axios.get(url, {
        timeout: 5000, //throw an error if the status code is 5xx or timeout
        validateStatus: status => status < 500
      });
      console.log(` - ${name} is reachable at ${url}`);
    } catch (error) {
      console.error(` - ${name} is not reachable at ${url}`, error.message);
      errorFlag = true;
    }
  }
}
const checkDbConnection = async () => {
  console.log("DB connection check....")
  const {DB_USER, DB_PASSWORD, DB_HOST} = process.env;
  try {
    const ginger2Con = await mysql.createConnection({
      host: DB_HOST, user: DB_USER, password: DB_PASSWORD, database: 'ginger2'
    });
    await ginger2Con.query('SELECT 1');
    console.log(' - Connected to Ginger 2 database');
    ginger2Con.destroy();
    const portalCon = await mysql.createConnection({
      host: DB_HOST, user: DB_USER, password: DB_PASSWORD, database: 'portail'
    });
    await portalCon.query('SELECT 1');
    console.log(' - Connected to Portal database');
    portalCon.destroy();
  } catch (error) {
    errorFlag = true;
    console.error(' - Error while connecting to the database', error.message);
  }
}

const checkConverterJsonMap = () => {
  //converter.json should be a json object with {role: [wroles]} each wrole is a wooch role check if it exist in "_help"."roles_values"
  //if not, print a warning
  let isValid = true
  console.log("Converter.json check...")
  const converter = require('../converter.json');
  const roles_values = converter['_help']['roles_values'];
  for (const [role, wroles] of Object.entries(converter.market)) {
    for (const wrole of wroles) {
      if (!roles_values.includes(wrole)) {
        isValid = false;
        console.error(`- "${wrole}" is not a valid wooch role for ${role}`);
      }
    }
  }
  if (isValid) console.log("Converter.json is valid")
  else {
    console.error("Converter.json is invalid, please check the roles values")
    errorFlag = true;
  }
}
const main = async () => {
  checkEnvVars();
  await checkUrls();
  await checkDbConnection();
  checkConverterJsonMap();
  if(errorFlag){
    console.error("\n\nDoctor found some issues, please fix them before running the bot....")
    throw "Some checks failed";
  }
}

module.exports ={
    play: () => main().then(() => process.exit(0)).catch((e) => process.exit(1))
}