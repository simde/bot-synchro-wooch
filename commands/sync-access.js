const wService = require('../services/wooch.service');
const pService = require('../services/portal.service');
const axios = require("axios");

const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

const syncAccess = async () => {
  console.log("Fetching users and roles from portal");
  const uARoles = await pService.getUsersAndRole();
  // FOR DEBUGGING
  //console.log("Successfully fetched users and roles from portal", JSON.stringify(uARoles));
  console.log("Fetching markets from wooch");
  const wMarkets = await wService.listMarkets();
  console.log("Fetching users with access from wooch");
  const idAndAccess = await wService.getAllUserIdWithAccess();
  // FOR DEBUGGING
  //console.log("Successfully fetched users with access from wooch", JSON.stringify(idAndAccess));

  console.log("Sync admin accesses !");
  const woochAdmins = idAndAccess.filter((u) => u.admin).map((u) => u.email);
  console.log('Wooch admins :', woochAdmins);
  const canBeAdmin = require('../converter.json').admin;
  //canBeAdmin is an object with {assos_slug: ["role can be admin"]}
  const portalAdmins = Object.keys(uARoles).filter((u) => {
    return Object.keys(uARoles[u].normal).some((a) => {
      return canBeAdmin[a] && canBeAdmin[a].some((r) => uARoles[u].normal[a].includes(r));
    });
  });
  //compare for add / delete operations
  const toAdd = portalAdmins.filter((u) => !woochAdmins.includes(u));
  const toRemove = woochAdmins.filter((u) => !portalAdmins.includes(u));
  console.group('Adding admin...');
  for (const email of toAdd) {
    try {
      console.log(`Adding user ${email}`);
      await wService.setAdmin(email, true);
    } catch (e) {
      console.error(`Error while adding user ${email} : ${e.response.data.message}`);
    }
  }
  console.groupEnd();
  console.group("Removing admin");
  for (const email of toRemove) {
    try {
      console.log(`Removing user admin ${email}`);
      await wService.setAdmin(email, false);
    } catch (e) {
      console.error(`Error while removing user ${email} : ${e.response.data.message}`);
    }
  }
  console.groupEnd();
  // remove users who have an access in wooch but are not in the portal
  const userToRemove = idAndAccess.filter((u) => !Object.keys(uARoles).includes(u.email));
  //remove accesses
  console.log('Removing access...', userToRemove.length, 'users');
  for (const user of userToRemove) {
    try {
      console.log(`Removing user access`, JSON.stringify(user));
      await wService.removeUserAccesses(user.email);
    } catch (e) {
      console.error(`Error while removing user ${JSON.stringify(user)} : ${e.response.data.message}`);
    }
  }
  // Reduce the array of markets to an object mapping market slugs to their IDs
  const loginToId = wMarkets.reduce((acc, m) => {
    acc[m.slug] = m.id;
    return acc;
  }, {});

  const toAddOrRefreshAccess = Object.entries(uARoles)
      .filter((portailUser) => {
        const woochUserAccess = idAndAccess.find((woochUser) => woochUser.email === portailUser[0]); // find the wooch access for the user based on its email address
        if (!woochUserAccess) return true; // if the user is in a asso and does not have access in wooch, we need to add it

        //check if the user has access to all the markets

        // get a string array of the markets IDs the user has access to in the portal
        const portalMarkets = Object
            .keys(portailUser[1].converted) // get the slugs of the markets the user has access to and his roles for each
            .map((assoSlug) => loginToId[assoSlug])  // convert the slugs to the market IDs
            .filter((m) => m !== undefined); // remove the undefined values (markets that do not exist in wooch)

        // get an array of strings containing the market IDs the user has access to in wooch
        const woochUserMarkets = woochUserAccess.accesses.map((a) => a.association_id); // replace each market accesses details with the market ID

        // returns true only if the user is in at least one asso (portail) but does not have the access in wooch
        return portalMarkets.some((portalMarket) => !woochUserMarkets.includes(loginToId[portalMarket]));
      })
      .reduce((acc, [uuid, roles]) => {
        acc[uuid] = roles;
        return acc;
      }, {});

  // FOR DEBUGGING
  //console.log('toAddOrRefreshAccess :', JSON.stringify(toAddOrRefreshAccess));

  console.log('Syncing ', Object.keys(toAddOrRefreshAccess).length, 'users');
  for (let [uuid, roles] of Object.entries(toAddOrRefreshAccess)) {
    try {
      console.log(`Syncing user ${uuid}`);
      const canContinue = await wService.removeUserAccesses(uuid).then((response) => {
        //get response header to check if we are rate limited
        if (response && response.headers && parseInt(response.headers['x-ratelimit-remaining']) < 10) {
          console.log('Rate limited, waiting 10s');
          return delay(10000).then(() => true);
        }
        return true;
      }).catch((err) => {
        if (err.response.status === 429) {
          console.log('Rate limited, waiting 1min');
          return delay(60000).then(() => axios.request(err.config)).then(() => true);
        }
        if (err.response.status !== 404) throw err;
        console.log(`User ${uuid} does not exist in wooch, skipping...`);
        return false;
      });
      if (!canContinue) continue;
      //add accesses
      for (const [slug, wRoles] of Object.entries(roles.converted)) {
        if (!loginToId[slug]) {
          console.error(`Error while syncing user ${uuid} : ${slug} does not exist in wooch (add the market first)`);
          continue
        }
        if (wRoles.length === 0) continue;
        console.log(`Adding access to ${slug} market for user ${uuid} with roles ${JSON.stringify(wRoles)}`);
        await wService.addUserAccess(uuid, loginToId[slug], wRoles).then((response) => {
          //get response header to check if we are rate limited
          if (response && response.headers && parseInt(response.headers['x-ratelimit-remaining']) < 10) {
            console.log('Rate limited, waiting 10s');
            return delay(10000);
          }
        }).catch((err) => {
          console.error(`Error while syncing user ${uuid} : ${err.response.data.message}`);
        });
        console.log(`Successfully added access to ${slug} market for user ${uuid}`);
      }
    } catch (e) {
      console.error(`Error while syncing user ${uuid} : ${e.message}`);
    }
  }
}

module.exports = {
    play: syncAccess
}