const wService = require('../services/wooch.service');
const pService = require('../services/portal.service');
const syncMarkets = async () => {
  console.log('Syncing markets...');
  const wMarkets = [], pMarkets = [];
  try {
    const markets = await wService.listMarkets();
    wMarkets.push(...markets);
  } catch (error) {
    console.error('Error while fetching markets from Wooch', error.response.data);
  }
  console.log(`${wMarkets.length} markets fetched from Wooch`);
  try {
    const markets = await pService.listAssociations();
    pMarkets.push(...markets.map(market => ({
      slug: market.login,
      name: market.shortname,
    })));
  } catch (error) {
    console.error('Error while fetching assos from Portal', error.response.data);
    process.exit(1);
  }
  console.log(`${pMarkets.length} assos fetched from Portal`);
  //now let's sync the markets to wooch, we will create the missing markets, and disable the ones that are not in the portal
    const missingMarkets = pMarkets.filter(pMarket => !wMarkets.find(wMarket => wMarket.slug === pMarket.slug));
    const disabledMarkets = wMarkets.filter(wMarket => !pMarkets.find(pMarket => wMarket.slug === pMarket.slug));
    const enabledMarkets = wMarkets.filter(wMarket => pMarkets.find(pMarket => wMarket.slug === pMarket.slug && !wMarket.active));
    console.log(`${missingMarkets.length} missing markets, ${disabledMarkets.length} markets to disable, ${enabledMarkets.length} markets to enable`);
    for (const market of missingMarkets) {
      try {
        await wService.createMarket(market.name, market.slug, market.slug + '@assos.utc.fr', "Marché de l'association " + market.name);
        console.log(`Market ${market.slug} created`);
      } catch (error) {
        console.error(`Error while creating market ${market.slug}`, error.response.data);
      }
    }
    for (const market of disabledMarkets) {
      try {
        await wService.editMarket(market.id, {
          active: false
        });
        console.log(`Market ${market.slug} disabled`);
      } catch (error) {
        console.error(`Error while disabling market ${market.slug}`, error);
      }
    }
    for (const market of enabledMarkets) {
      try {
        await wService.editMarket(market.id, {
          active: true
        });
        console.log(`Market ${market.slug} enabled`);
      } catch (error) {
        console.error(`Error while enabling market ${market.slug}`, error);
      }
    }
}

module.exports = {
  play: syncMarkets
}