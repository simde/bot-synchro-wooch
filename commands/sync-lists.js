const wService = require('../services/wooch.service');
const gService = require('../services/ginger.service');

const syncLists = async () => {
  console.log('Syncing lists...');
  const cotizlist = await gService.listCotizEmailsAndDates();
  console.log(`Sending cotiz list to wooch... ${cotizlist.length} users`);
  await wService.syncList(3, 'simde', cotizlist);
  console.log("Fetching Student list");
  const studentlist = await gService.listStudents();
  console.log(`Sending student list to wooch... ${studentlist.length} users`);
  await wService.syncList(4, 'simde', studentlist.map(s => ({email: s.email,start_date: new Date(Date.now())})));
}

module.exports = {
  play: syncLists
}