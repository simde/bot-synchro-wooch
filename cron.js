const cron = require('node-cron');
const wClient = require('./services/wooch.service');

const runSafe = (command) =>async () => {
  try {
    const start = Date.now();
    console.log(`Running ${command}`);
    const cmd = require;
    if (cmd.cache[command]) {
      delete cmd.cache[command];
    }
    await cmd(command).play();
    await wClient.setTaskState(`[BOT] ${command}`, 'success', `Done in ${Date.now() - start}ms`);
    console.log(`[${command}] Done in ${Date.now() - start}ms`);
  } catch (e) {
    console.error(`[${command}] Error while running command`, e);
    await wClient.setTaskState(`[BOT] ${command}`, 'failed', "Error while running command " + e.message.substring(0, 1000));
  }
}

console.log('Starting cron jobs');
//schedule every day at 1am
cron.schedule('0 1 * * *', runSafe("./commands/sync-markets"));
console.log(' - Sync markets scheduled every day at 1am');

//schedule every 2 hours
cron.schedule('0 */2 * * *', runSafe("./commands/sync-lists"));
console.log(' - Sync lists scheduled every 2 hours');

//schedule every thirty minutes
cron.schedule('*/30 * * * *', runSafe("./commands/sync-access"));
console.log(' - Sync access scheduled every thirty minutes');

//test run safe
console.log('Cron jobs started');