const commander = require('commander');

const program = new commander.Command();
program.version('1.0.0');

program.command('sync-markets').description("Sync associations to theirs associated markets").action(async ()=>require('./commands/sync-markets').play());


program.command('sync-access').description("Sync users and roles").action(async ()=>require('./commands/sync-access').play());
program.command('sync-lists').description( "Sync cotiz emails and dates").action(async ()=>require('./commands/sync-lists').play());
program.command('doctor').description("Check if the bot is ready to run").action(async ()=>require('./commands/doctor').play());
program.parse(process.argv);